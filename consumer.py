from kafka import KafkaConsumer
import psycopg2
import logging

consumer = KafkaConsumer(
    "demo-topic",
    auto_offset_reset="earliest",
    bootstrap_servers="kafka-project-joker-30f0.aivencloud.com:18912",
    client_id="demo-client-1",
    group_id="demo-group",
    security_protocol="SSL",
    ssl_cafile="ca.pem",
    ssl_certfile="service.cert",
    ssl_keyfile="service.key",
    api_version=(0, 10, 1),
)

logger = logging.getLogger('logger')
logger.setLevel(logging.DEBUG)
ch = logging.FileHandler('consumer.log')
ch.setLevel(logging.DEBUG)
strfmt = '[%(asctime)s] [%(name)s] [%(levelname)s] > %(message)s'
datefmt = '%Y-%m-%d %H:%M:%S'
formatter = logging.Formatter(fmt=strfmt, datefmt=datefmt)
ch.setFormatter(formatter)
logger.addHandler(ch)


global cursor
global connection
global raw_msgs
str1 = ["", "", ""]


def database_connection():
    global cursor
    global connection
    connection = psycopg2.connect(user="postgres",
                                  password="1111",
                                  host="127.0.0.1",
                                  port="5432")
    cursor = connection.cursor()


def receive_message():
    for tp, msgs in raw_msgs.items():
        for msg in msgs:
            logger.info("Received: {}".format(msg.value))
            print("Received: {}".format(msg.value))


def create_entry():
    global str1
    for tp, msgs in raw_msgs.items():
        for msg in msgs:
            str1 = msg.value.decode('utf-8').split(",")
            print(str1)
            if str1[0] is None or str1[1] is None or str1[2] is None:
                raise SystemExit("URL, topic or pattern must be fill")
            if float(str1[0]) < 0 or int(str1[1]) < 0 or (int(str1[2]) != 1 and int(str1[2]) != 0):
                raise SystemExit("Corrupt data")
            cursor.execute("INSERT INTO messages (RESPONSE_TIME, RESPONSE_CODE,PATTERN_MATCHED) VALUES "
                           "(%s, %s, %s)", (float(str1[0]), int(str1[1]), int(str1[2])))
            connection.commit()
            logger.info("запись успешно вставлена")


def main():
    global raw_msgs
    database_connection()
    while True:
        raw_msgs = consumer.poll()
        receive_message()
        create_entry()


if __name__ == "__main__":
    main()
