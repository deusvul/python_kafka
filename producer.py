from kafka import KafkaProducer
import requests
from requests import HTTPError
import time
import yaml
import logging
import os.path

producer = KafkaProducer(
    bootstrap_servers="kafka-project-joker-30f0.aivencloud.com:18912",
    security_protocol="SSL",
    ssl_cafile="ca.pem",
    ssl_certfile="service.cert",
    ssl_keyfile="service.key",
    api_version=(0, 10, 1),
)

logger = logging.getLogger('logger')
logger.setLevel(logging.DEBUG)
ch = logging.FileHandler('producer.log')
ch.setLevel(logging.DEBUG)
strfmt = '[%(asctime)s] [%(name)s] [%(levelname)s] > %(message)s'
datefmt = '%Y-%m-%d %H:%M:%S'
formatter = logging.Formatter(fmt=strfmt, datefmt=datefmt)
ch.setFormatter(formatter)
logger.addHandler(ch)

global msg
global settings
settings_path = 'settings.yaml'


def exists(path):
    if os.path.isfile(path) is False:
        raise SystemExit("File doesn't exist")


def read_settings(path):
    global settings
    with open(path) as parameters:
        settings = yaml.safe_load(parameters)
    if settings.get("url") is None or settings.get("pattern") is None or settings.get("topic") is None:
        raise SystemExit("URL, topic or pattern must be fill")
    print(settings)


def create_message(setting):
    global msg
    try:
        print(setting)
        resp = requests.get(setting.get("url"), timeout=2)
        print(resp)
        resp_time = resp.elapsed.total_seconds()
        msg = f'{resp_time},{resp.status_code}'
        print(msg)
        r = str(resp.text.encode("utf-8")).find("z")
        print(r)
        print(str(resp.content))
        if "D3" in resp.text:
            msg = f'{msg},1'
        else:
            msg = f'{msg},0'
    except ConnectionError:
        msg = f'0.0,0'
    except HTTPError:
        msg = f'0.0,1'
    print(msg)
    return msg


def send_messages(setting):
    print('Sending: message')
    logger.info('Sending: message')
    producer.send(setting.get("topic"), msg.encode("utf-8"))
    time.sleep(3)


def main():
    exists(settings_path)
    read_settings(settings_path)
    while True:
        create_message(settings)
        send_messages(settings)


if __name__ == "__main__":
    main()
